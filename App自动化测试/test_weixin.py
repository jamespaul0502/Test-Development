import os
import time
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.wait import WebDriverWait
import allure


@allure.feature('企业微信添加联系人')
@allure.title('企业微信添加联系人')
class TestDemo:

    def setup(self):
        # 启动前的配置
        caps = {
            'platformName': 'Android',  # 设备系统
            'platformVersion': '6.0',  # 指定安卓版本
            'deviceName': 'emulator-5554',  # 使用adb devices获取设备名称
            'appPackage': 'com.tencent.wework',  # 指定测试app的包的唯一名称
            'appActivity': '.launch.WwMainActivity',  # 指定进入app包的页面
            'noReset': True,  # 关闭应用更新、重置功能
            'unicodeKeyboard': True,  # 允许输入英文外的语言
            'resetKeyboard': True  # 重置输入法到初始状态
        }
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', caps)  # 发起请求
        self.driver.implicitly_wait(10)  # 设置隐性等待

    def teardown(self):
        self.driver.quit()  # 用例执行完成后关闭driver

    @allure.story('手动添加联系人')
    @allure.title('添加员工')
    @allure.description('通过手动输入联系人姓名和手机号添加成员，并判断是否添加成功')
    def test_01(self):
        # 显示等待xpath定位团队，点击，并将定位方式展现在测试报告
        with allure.step('点击团队'):
            loc1 = (MobileBy.XPATH, '//*[@class="android.widget.TextView" and @text="团队"]')
            WebDriverWait(self.driver, 50).until(lambda x: x.find_element(*loc1)).click()
            allure.attach(loc1[1], name='定位条件', attachment_type=allure.attachment_type.TEXT)
        # 显示等待xpath定位添加成员，点击，并将定位方式展现在测试报告
        with allure.step('点击添加成员'):
            loc2 = (MobileBy.XPATH, '//*[@text="添加成员..."]')
            WebDriverWait(self.driver, 50).until(lambda x: x.find_element(*loc2)).click()
            allure.attach(loc2[1], name='定位条件', attachment_type=allure.attachment_type.TEXT)
        # 显示等待resource-id定位手动输入添加，点击，并将定位方式展现在测试报告
        with allure.step('点击手动输入添加'):
            loc3 = (MobileBy.ID, 'com.tencent.wework:id/cfi')
            WebDriverWait(self.driver, 50).until(lambda x: x.find_element(*loc3)).click()
            allure.attach(loc3[1], name='定位条件', attachment_type=allure.attachment_type.TEXT)
        # 显示等待resource-id定位输入姓名和输入手机号文本框，输入内容，并将定位方式展现在测试报告
        with allure.step('输入姓名和手机号'):
            user_name = (MobileBy.ID, 'com.tencent.wework:id/ays')
            WebDriverWait(self.driver, 50).until(lambda x: x.find_element(*user_name)).send_keys('张景德')
            phone = (MobileBy.ID, 'com.tencent.wework:id/f4m')
            self.driver.find_element(*phone).send_keys('18811755659')
            allure.attach(f'姓名：{user_name}、手机号：{phone}', name='定位条件', attachment_type=allure.attachment_type.TEXT)
        # resource-id定位保存按钮，点击，并将定位方式展现在测试报告
        with allure.step('点击保存'):
            loc6 = (MobileBy.ID, 'com.tencent.wework:id/ac9')
            self.driver.find_element(*loc6).click()
            allure.attach(loc6[1], name='定位条件', attachment_type=allure.attachment_type.TEXT)
        # xpath定位Toast控件，获取内容，断言是否和预期结果一致
        with allure.step('获取Toast内容，判断添加是否成功'):
            loc7 = (MobileBy.XPATH, '//*[@class="android.widget.Toast"]')
            result = self.driver.find_element(*loc7).text
            assert result == '添加成功'
            allure.attach(loc7[1], name='定位条件', attachment_type=allure.attachment_type.TEXT)
        time.sleep(3)


if __name__ == '__main__':
    os.system('pytest test_qywx.py -vs --alluredir=./result')
    os.system('allure generate ./result -o ./report --clean')