import requests
from hamcrest import *
import pytest


class TestAPI(object):
    def test_get(self):
        '''
        get请求
        :return:
        '''
        params = {'name': 'Paul'}
        r = requests.get('https://httpbin.ceshiren.com/get', params=params)
        print(r.json())
        # 断言parmas数据是否在respones响应中
        assert_that(r.json()['args']['name'], equal_to('Paul'))

    def test_post(self):
        '''
        post请求
        :return:
        '''
        headers = {
            'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate',
            'Content-Length': '15',
            'Content-Type': 'application / json', 'Host': 'httpbin.ceshiren.com',
            'User - Agent': 'python - requests / 2.25.1',
            'X - Forwarded - Host': 'httpbin.ceshiren.com',
            'X - Scheme': 'https'}
        json = {
            'name': "Paul",
            'password': '123456'
        }
        r = requests.post('https://httpbin.ceshiren.com/post', json=json, headers=headers)
        print(r.json()['json'])
        assert_that(r.json()['json']['name'], equal_to('Paul'))
        assert_that(r.json()['json']['password'], equal_to('123456'))


if __name__ == '__main__':
    test = TestAPI()
