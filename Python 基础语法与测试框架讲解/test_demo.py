import io

import pytest
import yaml


class TestDemo():
    def setup_class(self):
        """
        ====整个测试类开始前只执行一次setup_class====
        """
        pass

    def teardown_class(self):
        """
        ====整个测试类结束后只执行一次teardown_class====
        """
        pass

    def setup_method(self):
        """
        ====类里面每个用例执行前都会执行setup_method==
        """
        print("【开始计算】")

    def teardown_method(self):
        """
        ==类里面每个用例结束后都会执行teardown_method==
        """
        print("【计算结束】")

    @pytest.mark.parametrize("a, b, c", yaml.safe_load(open('test_addition.yml','r')))
    def test_addition(self, a, b, c):
        assert a + b == c, f"判断 {a} 加上 {b} 是否等于 {c}"

    @pytest.mark.parametrize("a, b, c", yaml.safe_load(open('test_division.yml','r')))
    def test_division(self, a, b, c):

        assert a / b == c, f"判断 {a} 除以 {b} 是否等于 {c}"


if __name__ == '__main__':
    pytest.main(['-v'])