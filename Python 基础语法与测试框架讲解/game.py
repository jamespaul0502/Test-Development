from random import randint


class Timo(object):
    def __init__(self, name='timo', hp=120, power=randint(20, 35)):
        # 英雄血量
        self.hp = hp
        # 英雄攻击力
        self.power = power
        # 英雄名称
        self.name = name

    def fight(self, enemy_hp, enemy_power):
        # 英雄最终的⾎量 = 英雄hp属性 - 敌⼈的攻击⼒enemy_power
        self.hp = self.hp - enemy_power
        # 敌⼈最终的⾎量 = 敌⼈的⾎量enemy_hp - 英雄的power属性
        enemy_final_hp = enemy_hp - self.power

        return enemy_final_hp


class Police(object):
    def __init__(self, name='police', hp=100, power=randint(40, 55)):
        # 英雄血量
        self.hp = hp
        # 英雄攻击力
        self.power = power
        # 英雄名称
        self.name = name

    def fight(self, enemy_hp, enemy_power):
        # 英雄最终的⾎量 = 英雄hp属性 - 敌⼈的攻击⼒enemy_power
        self.hp = self.hp - enemy_power
        # 敌⼈最终的⾎量 = 敌⼈的⾎量enemy_hp - 英雄的power属性
        enemy_final_hp = enemy_hp - self.power

        return enemy_final_hp


def playGame():
    while True:
        hero = input("请选择你使用的英雄('请输入数字1或2')\n1、police\n2、timo\n")
        # 判断用户输入是否为数字1或2
        if hero.isdigit():
            if hero == "1":
                my_hero = Police()
                robot_hero = Timo()
                break
            elif hero == "2":
                my_hero = Timo()
                robot_hero = Police()
                break
            else:
                print("输入错误，只能输入数字1或数字2")
        else:
            print("输入错误，只能输入数字1或数字2")
    print(f"你选择的英雄为{my_hero.name},攻击力为{my_hero.power},血量为{my_hero.hp}")
    while True:
        print("开始游戏")
        # 获取敌人血量
        robot_hero.hp = my_hero.fight(robot_hero.hp, robot_hero.power)
        # 判读敌人和英雄血量大小，判断游戏胜利
        if my_hero.hp > robot_hero.hp:
            print(f"英雄剩余血量为{my_hero.hp},敌人血量为{robot_hero.hp},敌人攻击力为{robot_hero.power}")
            print("英雄获胜")
            break
        elif my_hero.hp < robot_hero.hp:
            print(f"英雄剩余血量为{my_hero.hp},敌人血量为{robot_hero.hp},敌人攻击力为{robot_hero.power}")
            print("敌人获胜")
            break
        else:
            print(f"英雄剩余血量为{my_hero.hp},敌人血量为{robot_hero.hp},敌人攻击力为{robot_hero.power}")
            print("平局")
            break


if __name__ == '__main__':
    playGame()
