import yaml
import io
from loguru import logger


def _load_yaml_file(yaml_file):
    """ 加载yaml文件并检查文件内容格式
    """
    with io.open(yaml_file, 'r', encoding='utf-8') as stream:
        try:
            yaml_content = yaml.load(stream)
            logger.info(f"yaml文件读取的内容为\n{yaml_content}")
        except yaml.YAMLError as ex:
            logger.error(str(ex))
        return yaml_content