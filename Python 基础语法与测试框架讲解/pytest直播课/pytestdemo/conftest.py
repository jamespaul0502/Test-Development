#!/usr/env/bin python
# -*- coding: utf-8 -*-

import os
import platform
from datetime import datetime

from loguru import logger
import pytest

from pytestdemo.calculator import Calculator

"""
1、改造 计算器 测试用例，使用fixture函数获取计算器的实例
2、计算之前打印开始计算，计算之后打印结束计算
3、添加用例日志，并将日志保存到日志文件目录下
4、生成测试报告，展示测试用例的标题，用例步骤，与测试日志，截图附到课程贴下
"""
separator = '\\' if platform.system() == 'Windows' else '/'


# 定义一个fixture函数:实例化一个计算器对象并返回
@pytest.fixture()
def calc_demo():
    # 实例化一个计算器对象
    calc1 = Calculator()
    # 返回计算器对象calc
    return calc1


@pytest.fixture(scope='package', autouse=True)
def get_logger():
    log_path = os.path.dirname(
        os.path.realpath(__file__)) + separator + "logs" + separator + 'runtime_{time}.log'
    logger.add(log_path, rotation="00:00", level="DEBUG")
    logger.info("初始化loger成功")
    # return logger




