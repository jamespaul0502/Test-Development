import os
import pytest
import allure
from loguru import logger
from pytestdemo.util.yaml import _load_yaml_file


data_path = './data/calc.yml'


@allure.feature("计算器测试类")
class TestCalc:
    def setup_class(self):
        self.loger = logger

    @allure.title('{a} + {b} = {c}')
    @allure.story('加法测试')
    @allure.description('该用例包含计算机正向和反向的测试')
    @pytest.mark.parametrize("a,b,c", _load_yaml_file(data_path)['add']['datas'], ids=_load_yaml_file(data_path)['add']['myid'])
    def test_add(self, calc_demo, a, b, c):
        # 调用fixture生成的计算器实例，传参
        with allure.step('调用计算器类方法'):
            result = calc_demo.add(a, b)
            self.loger.info(f'计算{a}+{b}')
        # 判断计算结果是不是小数，如果是，则保留两位数
        with allure.step('浮点数尾数保留'):
            if isinstance(result, float):
                result = round(result, 2)
            self.loger.info('转换小数点，保留两位浮点数')
        # 对计算结果和预期结果进行断言
        with allure.step('断言'):
            assert result == c
            self.loger.info(f'判断{a}+{b}是否等于{c}')
        allure.attach('测试用例执行完成', name='test_result', attachment_type=allure.attachment_type.TEXT)


if __name__ == '__main__':
    pytest.main(
            ['-s', '-v', 'test_calc.py', '--html=../report/report.html', '--alluredir', '../report/allure-results'])
    # os.system('pytest test_calc.py -vs --alluredir=./result')
    # os.system('allure generate ./result -o ./report --clean')
