import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class TestWeiXin():
    def setup_method(self):
        # webdriver路径
        webdriver_path = 'E:\\PythonProject\\HttpRunnerManager-master\\UiManager\\utils\\UIRunner\\tools\\chromedriver.exe'
        options = webdriver.ChromeOptions()
        # options.debugger_address = '127.0.0.1:9222'
        options.binary_location = "C:\Program Files\Google\Chrome\Application\chrome.exe"
        self.driver = webdriver.Chrome(executable_path=webdriver_path, chrome_options=options)
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def teardown_method(self):
        self.driver.close()
        self.driver.quit()

    def test_login(self):
        # self.driver.get('https://work.weixin.qq.com/wework_admin/frame')
        # time.sleep(4)
        # cookies = self.driver.get_cookies()
        # print(cookies)
        cookies = [
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': False,
                'name': 'wwrtx.vid',
                'path': '/',
                'secure': False,
                'value': '1688850793127641'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': True,
                'name': 'wwrtx.vst',
                'path': '/',
                'secure': False,
                'value': 'a9jPrWHs7aUu8J4Gw1gl0UB7bQ-5YDqWscRdhn_4sJ38eE-h8gsnDHJ2qq2PLoUal6HQ9Vi_mFGQlwEbPruM2GPOWyd7zMmO6u0bm06emzj_zqEtLK-5NEfXEzw4-itXOMjHwB6lW3A_DS6Dv6QgLd9aQTd_s8I1n3C5oA9dG8m9k6ouIeWh4Y7MP0NtPxWTlhSGFAE8r-ZfJNfbU9imao5rnam4CsuWuanyJIgW5_TsrBgnQPFF7MX4L_zRKC0ZKGSEK9FwhginF2CyxaCJSg'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': False,
                'name': 'wxpay.vid',
                'path': '/',
                'secure': False,
                'value': '1688850793127641'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': False,
                'name': 'wxpay.corpid',
                'path': '/',
                'secure': False,
                'value': '1970325049448644'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': True,
                'name': 'wwrtx.ref',
                'path': '/',
                'secure': False,
                'value': 'direct'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': True,
                'name': 'wwrtx.ltype',
                'path': '/',
                'secure': False,
                'value': '1'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': False,
                'name': 'wwrtx.d2st',
                'path': '/',
                'secure': False,
                'value': 'a2936213'
            },
            {
                'domain': 'work.weixin.qq.com',
                'expiry': 1621286707.449414,
                'httpOnly': True,
                'name': 'ww_rtkey',
                'path': '/',
                'secure': False,
                'value': '8tba6n6'
            },
            {
                'domain': '.qq.com',
                'expiry': 1621341577,
                'httpOnly': False,
                'name': '_gid',
                'path': '/',
                'secure': False,
                'value': 'GA1.2.485419341.1621255172'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': True,
                'name': 'wwrtx.refid',
                'path': '/',
                'secure': False,
                'value': '7077226152371342'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': True,
                'name': 'wwrtx.sid',
                'path': '/',
                'secure': False,
                'value': 'TEnqsW1e2u4ZoUMJsdDPE7ZYpjTAI61XE-2Ip2qRIJIPTfVoXmu-bAVkuSxR_Xdz'
            },
            {
                'domain': '.work.weixin.qq.com',
                'httpOnly': False,
                'name': 'wwrtx.cs_ind',
                'path': '/',
                'secure': False,
                'value': ''
            },
            {
                'domain': '.qq.com',
                'expiry': 1621255232,
                'httpOnly': False,
                'name': '_gat',
                'path': '/',
                'secure': False,
                'value': '1'
            },
            {
                'domain': '.qq.com',
                'expiry': 1684327177,
                'httpOnly': False,
                'name': '_ga',
                'path': '/',
                'secure': False,
                'value': 'GA1.2.226501419.1621255172'
            },
            {
                'domain': '.work.weixin.qq.com',
                'expiry': 1652791171,
                'httpOnly': False,
                'name': 'wwrtx.c_gdpr',
                'path': '/',
                'secure': False,
                'value': '0'
            },
            {
                'domain': '.work.weixin.qq.com',
                'expiry': 1623847180,
                'httpOnly': False,
                'name': 'wwrtx.i18n_lan',
                'path': '/',
                'secure': False,
                'value': 'zh'
            }
        ]
        for cookie in cookies:
            # if 'expiry' in cookie.keys():
            #     cookie.pop('expiry')
            self.driver.add_cookie(cookie_dict=cookie)
        time.sleep(1)
        self.driver.get('https://work.weixin.qq.com/wework_admin/frame')
        time.sleep(2)
        assert self.driver.current_url == 'https://work.weixin.qq.com/wework_admin/frame'


if __name__ == '__main__':
    pytest.main(['-v'])
